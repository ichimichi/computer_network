//client
#include<stdio.h>
#include<stdlib.h>
#include<string.h>

#include<sys/types.h>
#include<sys/socket.h>

#include<netinet/in.h>
#include<unistd.h>
#include<arpa/inet.h>

int main()
{
	char buff[256];
	int n_socket;


	
	if( (n_socket = socket(AF_INET,SOCK_STREAM,0)) < 0){
		printf("Error : Socket creation unsuccesful.\n");
		return 0;
	}

	struct sockaddr_in client;
	
	client.sin_family = AF_INET;
	client.sin_port = htons(9002);
	client.sin_addr.s_addr = inet_addr("0.0.0.0");


		if(connect(n_socket,(struct sockaddr *) &client, sizeof(client)) <0){
			printf("Error : Connection failed.");
			return 0;
		}
	


		printf("\n[client] Enter Marks : ");
		scanf("%[^\n]s", buff);	
	
		if(send(n_socket, buff,strlen(buff),0)<0)
		{
			printf("Error : Sending failed.\n");
			return 0;
		}


		if((recv(n_socket, buff, strlen(buff) , 0 ))< 0){
			printf("Error: Message receiving failed.\n");
			return 0;
		}
		printf("[server] GRADE : %s\n",buff);

	


	close(n_socket);
	return 0;
}
