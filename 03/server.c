//server
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<sys/socket.h>
#include<sys/types.h>
#include<arpa/inet.h>

#include<netinet/in.h>
#include<unistd.h>


int main(){
	int s_socket;
	char buff[256];
	char grade[4];


	if((s_socket = socket(AF_INET, SOCK_STREAM, 0))<0){
		printf("Error: Socket creation failed.\n");
		return 0;
	}
	
	struct sockaddr_in server;
	
	server.sin_family = AF_INET;
	server.sin_port = htons(9002);
	server.sin_addr.s_addr = htonl(INADDR_ANY);

	bind(s_socket, (struct sockaddr*) &server, sizeof(server));

	int c_socket;
	
	listen(s_socket, 10);

	
	c_socket = accept(s_socket,(struct sockaddr*) NULL, NULL);
			
			close(s_socket);
			bzero(buff,256);

			if(recv(c_socket, buff , 256 , 0) > 0)
			{	
				//printf("->client : %s",buff);

				int marks=atoi(buff);
				if(marks>=85){
					strcpy(grade,"A");
				}else if(marks>=75)
				{
					strcpy(grade,"B");
				}else if(marks >=60)
				{
					strcpy(grade,"C");
				}
				else if (marks>=50)
				{
					strcpy(grade,"D");
				}else{
					strcpy(grade,"FAIL");
				}

				send(c_socket, grade, sizeof(grade), 0);

			}
			else{
				printf("Error : Receiving Message Failed\n");
			}

	close(c_socket);
	return 0;
}
