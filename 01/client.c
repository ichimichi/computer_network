//client
#include<stdio.h>
#include<stdlib.h>

#include<sys/types.h>
#include<sys/socket.h>

#include<netinet/in.h>
#include<unistd.h>

int main(){
	//create a socket
	int network_socket;
	network_socket = socket(AF_INET,SOCK_STREAM,0);

	// specifying an address for the socket
	struct sockaddr_in server_address;
	server_address.sin_family = AF_INET;
	server_address.sin_port = htons(9002);
	server_address.sin_addr.s_addr = INADDR_ANY;

	int connection_status = connect(network_socket,(struct sockaddr *) &server_address, sizeof(server_address));
	if(connection_status == -1){
		printf("There was an error making a connection to the server\n\n");
	}
	else
	{
	char client_message[256];
	char server_response[256];

	//sending message to the server 
	printf("Enter a message to send to the server: ");
	scanf("%[^\n]s", client_message);
	
	send(network_socket, client_message,sizeof(client_message),0); 
	
	
	// receive data from the server
	recv(network_socket, &server_response, sizeof(server_response), 0 );


	printf("->server : %s\n\n", server_response);

	
	}
	close(network_socket);
	return 0;
}
