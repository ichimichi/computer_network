#include <stdio.h>
#include <unistd.h>


main(int argc, char const *argv[])
{
    int x=1;
    int returnValue = fork();
    if(returnValue == 0){
        //only child process executes this
        printf("child, x = %d\n",++x);
    }else{
        //only parent executes this
        printf("parent, x = %d\n",--x);
    }
    //parent and child execute this
    printf("Exiting with x = %d\n",x);
    return 0;
}
