#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/wait.h>

main(int argc, char const *argv[])
{
    pid_t childPIDorZero = fork();
    if(childPIDorZero<0){
        perror("fork() error");
        exit(-1);
    }

    if(childPIDorZero != 0){
        printf("I'm the parent %d, my child is %d\n",getpid(),childPIDorZero);
        wait(NULL); //wait for child process to join with this parent
    }else{
        printf("I'm the child %d, my parent is %d\n",getpid(),getppid());
        execl("/bin/echo","echo","Hello world",NULL);
    }

    
    return 0;
}
