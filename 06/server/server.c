#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <unistd.h>

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int main(int argc, char *argv[])
{
    int s_socket;
    int client;

    s_socket = socket(AF_INET, SOCK_STREAM, 0);

    struct sockaddr_in address;
    int port = atoi(argv[1]);
    address.sin_family = AF_INET;
    address.sin_port = htons(port);
    address.sin_addr.s_addr = INADDR_ANY;

    if (bind(s_socket, (struct sockaddr *)&address, sizeof(address)) != 0)
    {
        perror("ERROR: bind");
        exit(1);
    }

    if (listen(s_socket, 5) != 0)
    {
        perror("ERROR: listen");
        exit(1);
    }

    struct sockaddr_in c_address;
    socklen_t c_address_size;
    if ((client = accept(s_socket, (struct sockaddr *)&c_address, &c_address_size)) < 0)
    {
        perror("ERROR: accept");
        exit(1);
    }

    char ip[INET_ADDRSTRLEN];
    inet_ntop(AF_INET, (struct sockaddr *)&c_address, ip, sizeof(ip));
    printf("%s connected\n", ip);

    char msg[1024] = "Please Enter a file name : ";
    if (send(client, msg, strlen(msg), 0) < 0)
    {
        perror("ERROR: send");
    }

    char request[1024];
    memset(request, '\0', sizeof(request));

    int n = recv(client, request, sizeof(request), 0);
    request[n - 1] = '\0';
    fputs(request, stdout);

    FILE *file;

    if (access(request, F_OK) != -1)
    {
        memset(msg, '\0', sizeof(msg));
        strcat(msg, "file found");
        if (send(client, msg, strlen(msg), 0) < 0)
        {
            perror("ERROR: send");
        }
        file = fopen(request, "rb");
        if (file != NULL)
        {

            while (1)
            {
                unsigned char fileBuffer[1024] = {0};
                int bytes = fread(fileBuffer, 1, 1024, file);

                if (bytes > 0)
                {
                    write(client, fileBuffer, bytes);
                }
                if (bytes < 1024)
                {
                    printf("File transfer completed\n");
                    break;
                }
            }
        }

        fclose(file);
    }
    else
    {
        memset(msg, '\0', sizeof(msg));
        strcat(msg, "file not found");
        if (send(client, msg, strlen(msg), 0) < 0)
        {
            perror("ERROR: send");
        }
    }

    close(s_socket);
    close(client);
}