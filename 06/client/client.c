#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>
#include <arpa/inet.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char *argv[])
{
    int my_socket = socket(AF_INET, SOCK_STREAM, 0);

    int port = atoi(argv[1]);
    struct sockaddr_in s_address;

    s_address.sin_family = AF_INET;
    s_address.sin_port = htons(port);
    s_address.sin_addr.s_addr = INADDR_ANY;

    if (connect(my_socket, (struct sockaddr *)&s_address, sizeof(s_address)) < 0)
    {
        perror("Error: socket");
        exit(1);
    }

    char ip[INET_ADDRSTRLEN];
    inet_ntop(AF_INET, (struct sockaddr *)&s_address, ip, sizeof(ip));

    printf("Connected to server %s\n", ip);
    char msg[1024];

    memset(msg, '\0', sizeof(msg));
    recv(my_socket, msg, sizeof(msg), 0);
    fputs(msg, stdout);

    char request[1024];
    memset(request, '\0', sizeof(request));
    fgets(request, sizeof(request), stdin);

    int n = write(my_socket, request, strlen(request));
    request[n - 1] = '\0';
    
    memset(msg, '\0', sizeof(msg));
    recv(my_socket, msg, sizeof(msg), 0);
    fputs(msg, stdout);

    if (strncmp(msg, "file found", 10) == 0)
    {
        FILE *file;

        printf("File Name: %s\n", request);
        printf("Receiving file...");
        file = fopen(request, "ab");
        if (file  == NULL)
        {
            printf("Error opening file");
            return 1;
        }
        
        int bytes = 0;
        char fileBuffer[1024];

        while ((bytes = read(my_socket, fileBuffer, 1024)) > 0)
        {
            fwrite(fileBuffer, 1, bytes, file);
        }

        printf("\nFile download successfully completed\n");
    }

    close(my_socket);
}
