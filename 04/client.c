#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <unistd.h>

#include <sys/types.h>
#include <sys/socket.h>

#include <netinet/in.h>
#include <netdb.h>

void error(const char *msg)
{
    printf(msg);
    exit(0);
}

int main(int argc, char *argv[])
{
    char buffer[1024];

    
    if (argc < 3)
    {
        printf( "Please excute in this format %s {server address} {port no}\n", argv[0]);
        exit(0);
    }

    int portno = atoi(argv[2]);
    
    int s_socket = socket(AF_INET, SOCK_STREAM, 0);

    if (s_socket < 0){
        error("ERROR: opening socket");
    }
        
    struct hostent *server = gethostbyname(argv[1]);
    if ( server == NULL)
    {
        fprintf(stderr, "ERROR: No such server running\n");
        exit(0);
    }

    

    struct sockaddr_in s_address;
    //bzero((char *)&s_address, sizeof(s_address));

    s_address.sin_family = AF_INET;
    bcopy((char *)server->h_addr_list[0], (char *)&s_address.sin_addr.s_addr, server->h_length);
    s_address.sin_port = htons(portno);
    
    if (connect(s_socket, (struct sockaddr *)&s_address, sizeof(s_address)) < 0)
        error("ERROR: connecting");

    
    int n = read(s_socket, buffer, 1024);
    if (n < 0)
        error("ERROR: reading from socket");

    printf("%s\n", buffer);
    bzero(buffer, 1024);
    printf("[client] Enter a number: ");
    
    
    fgets(buffer, 1024, stdin);


    n = send(s_socket, buffer, strlen(buffer),0);
    bzero(buffer, 1024);
    if (n < 0)
        error("ERROR: sending to socket");

    n = read(s_socket, buffer, 1024);
    if (n < 0)
        error("ERROR: reading from socket");
    printf("[server] : %s\n", buffer);
    bzero(buffer, 1024);

    close(s_socket);
    return 0;
}