#include <stdio.h> 
#include <string.h>
#include <stdlib.h> 

#include <unistd.h>
#include <sys/types.h> 
#include <sys/socket.h> 

#include <arpa/inet.h>
#include <netinet/in.h> 
#include <sys/select.h>

#include<errno.h>

#define PORT 9002
#define MAX 30
    
void error(const char *msg)
{
    printf(msg);
    exit(0);
}
int main()  
{  
    int new_socket , client_socket[30];
    int max_sd,i;  
    struct sockaddr_in address;  
        
    char buffer[1025];
    bzero(buffer, 1024);

    fd_set readfds;  
    
    strcpy(buffer,"[server] : Connection Successful");  

    for (i = 0; i < MAX; i++)  
    {  
        client_socket[i] = 0;  
    }

    int s_socket = socket(AF_INET , SOCK_STREAM , 0);
    if( s_socket < 0)
        error("ERROR: opening socket");

    address.sin_family = AF_INET;  
    address.sin_addr.s_addr = INADDR_ANY;  
    address.sin_port = htons( PORT );  
         
    if (bind(s_socket, (struct sockaddr *)&address, sizeof(address))<0)
        error("ERROR: bind failed");

    printf("Listener on port %d\n", PORT);  
        
    if (listen(s_socket, 5) < 0)  
        error("ERROR: listen");

    int addrlen = sizeof(address);  
    puts("Waiting for connections ...");  
        
    while(1)  
    {  
        FD_ZERO(&readfds);  
    
        //add server socket to set 
        FD_SET(s_socket, &readfds);  
        max_sd = s_socket;  
            
        //add child sockets to set 
        for ( i = 0 ; i < MAX ; i++)  
        {  
            //if valid socket descriptor then add to read list 
            if(client_socket[i] > 0)  
                FD_SET( client_socket[i] , &readfds);  
                
            //highest file descriptor number, need it for the select function 
            if(client_socket[i] > max_sd)  
                max_sd = client_socket[i];  
        }  
    
        //wait for an activity on one of the sockets , timeout is NULL
        int activity = select( max_sd + 1 , &readfds , NULL , NULL , NULL);  
      
        if ((activity < 0) && (errno!=EINTR))
            printf("ERROR: select error\n");  
            
        //If something happened on the server socket , 
        //then its an incoming connection 
        if (FD_ISSET(s_socket, &readfds))  
        {  
            if ((new_socket = accept(s_socket,(struct sockaddr *)&address, (socklen_t*)&addrlen))<0)  
                error("ERROR: accept");
            
            printf("New connection , socket fd is %d , ip is : %s , port : %d \n" , new_socket , inet_ntoa(address.sin_addr) , ntohs(address.sin_port));  
          
            if( send(new_socket, buffer, 1024, 0) == 0 )
                error("ERROR: sending to socket"); 
                
            //add new socket to array of sockets 
            for (i = 0; i < MAX; i++)  
            {  
                //if position is empty 
                if( client_socket[i] == 0 )  
                {  
                    client_socket[i] = new_socket;  
                    printf("Adding to list of sockets as %d\n" , i);  
                        
                    break;  
                }  
            }  
        }  
            
        //else its some IO operation on some other socket
        for (i = 0; i < MAX; i++)  
        {        
            if (FD_ISSET(client_socket[i] , &readfds))  
            {  
                if (read(client_socket[i] , buffer, 1024) == 0)  
                {  
                    //Somebody disconnected , get his details and print 
                    getpeername(client_socket[i] , (struct sockaddr*)&address , (socklen_t*)&addrlen);  
                    printf("Host disconnected , ip %s , port %d \n" , inet_ntoa(address.sin_addr) , ntohs(address.sin_port));  
                    close(client_socket[i] );  
                    client_socket[i] = 0;  
                }  
                else
                {   
                    float n = atoi(buffer);
                    n=n*n;
                    gcvt(n,5,buffer);
                    send(client_socket[i] , buffer , 1024 , 0 );  
                }  
            }  
        }  
    }  
        
    return 0;  
}  